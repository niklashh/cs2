# Inferno Smoke Lineups

## CT-side

### Ramp

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2115062479288256941/49C6E0E04032C3AC1C4C91D0AEA5135BE5B587C9/){ .zoom3x }
<figcaption>JT, aim at center of the dark spot, very low tolerance</figcaption>
</figure>

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098998209/5555F72F82AF2EC7360523A022290B339E3A9678/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ width="50%" }

### Mid

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009099040072/5969DF6CCC02F4063D2165E93569232F3823128F/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom3x }
<figcaption>JT</figcaption>
</figure>

!!! note

    - There is a peg poking out of the wood under the crosshair
    - Takes 8s to land so T's may have time to rush through it

<div class="grid" markdown>

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009099040303/E107E99AD9DAD0FA1B5990F00DD0586037EB6562/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009099039890/F0EEBAC52077CB77906C34C5CBE666345E67AC19/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom1_5x }

</div>

### Banana

![](https://steamuserimages-a.akamaihd.net/ugc/2115062479287598376/CEF49D1102A4C94E5651EE7E29878FD6CBB4A099/){ .zoom2x }

### Banana (retake)

![](https://steamuserimages-a.akamaihd.net/ugc/2115062479288419924/DCD1D7BE680A58A9430EC5CA0382DAFAF5C5C090/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom2x }

### Quad/newbox

May be useful during a retake

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2115062479288418840/3138E62A640CEC13438F8E6F926AB01D33A30EC7/){ .zoom2x }
<figcaption>JT</figcaption>
</figure>

<div class="grid" markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2115062479288419187/935D30E1C58889C5C390839353921BF9E7C1BB80/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)

![](https://steamuserimages-a.akamaihd.net/ugc/2115062479288418663/92346AABB654DFEBE6A95EB805AA3CF8C851267C/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)
</div>

## T-side

### Coffin

![](https://steamuserimages-a.akamaihd.net/ugc/2115062479287598140/9409A82589765CCA05AD55AA952D9E31628850A7/)

<div class="grid" markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2115062479287597816/12078E435BC5DB24E0EDADF370683EBBCE1592E9/)

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2115062479287597647/D5B0EC68D83D7DA12849866E863359C905A98B63/)
<figcaption>Watch out for this spot</figcaption>
</figure>
</div>

### B site CT from alt mid

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098991816/2C1F1B275EDE2A577629E0E88EF63850E5C21724/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)
<figcaption>JT</figcaption>
</figure>

!!! note

    - Crouch jump throw to land quietly on the ledge in front
    - Blocks vision to grill, where a CT might be lurking

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098991631/E662A2778C64EC7AD10BDE861EAE596D4459DD5F/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ width="50%" }
