# Mirage Lineup Index

## CT-side

## T-side

<div class="radar-container">
<img src="https://static.wikia.nocookie.net/cswikia/images/9/95/Cs2_mirage_radar.png" class="noglightbox" style="pointer-events:none;">

<a href="./mir_smokes.html#ct" style="top:79%;left:45%;" class="t-smoke"></a>
<a href="./mir_smokes.html#upper-connector" style="top:57%;left:50%;" class="t-smoke"></a>
</div>
