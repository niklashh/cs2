# Overpass Firebomb Lineups

## CT-side

## T-side

### Short

1. Hold down left+right while approaching alley
2. When turning the corner aim at rightmost point of **B**
   ![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098989883/9B62933852892C7E786DCEA1B4864F76BC8D21A8/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom2x }
3. Keep aiming at the same point. Jump throw when passing the center of the shadow
   ![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098989629/269376B72B21115BC04BAA7CB8D6F6867A059E9A/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098989411/993361E23169041D496CA938390794DB7300EF06/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom1_5x width="50%" }

### Close left

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009099040730/3EF133B52E9778926627E99324C1107465AB3684/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom2x }
<figcaption>Aim at the darker group of leaves</figcaption>
</figure>
