# Inferno Flash Lineups

## T-side

### Boiler

![](https://steamuserimages-a.akamaihd.net/ugc/2115062479288420174/655C60F53D0A1F4E93848DF7B839D393680909B5/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom2x }

> Does not flash team mates coming from mid, but also misses the near corner on long
