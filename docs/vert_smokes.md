# Vertigo Smoke Lineups

## CT-side

### Ramp

!!! todo

## T-side

### Boost

- Blocks heaven and boost
- Does not block top of boost

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009099001339/5CF3C0D669A341C20CDBDAF28AA2A603F4CCEB86/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom1_5x }
<figcaption>Medium tolerances</figcaption>
</figure>

<div class="grid" markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009099001508/C91531CF906CC2FCE9098E469D99D6AFC3346131/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009099001161/6D9A0CB58E1FB52D18B35C54F34520D4BBFE3361/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009099001002/44507D94EC12CA347F7EEF08BCB5148B00E21D50/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)
<figcaption>Watch out for this angle</figcaption>
</figure>
</div>

### Heaven

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009099000475/011A019D0CFDBF21F54F3B800B4F42F49CD26DE0/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom2x }
<figcaption>JT</figcaption>
</figure>
