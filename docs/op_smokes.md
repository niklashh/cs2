# Overpass Smoke Lineups

## CT-side

### Monster

!!! todo

### A main right

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098986389/57D725B1F44638509C3D366B58B740F5FBD71999/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom1_5x }

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098554077/481CA25AD0BF8A4B3557B16F67144E1E5B9BBD15/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)
<figcaption>Toilet and banana are somewhat visible from top of green</figcaption>
</figure>

### A main left

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098553899/5F32773C14E06A5903B7FB81ACD3D205D41B11F1/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)
<figcaption>Align the side indicator below the sign with horizontal centering</figcaption>
</figure>

Together with [A main right](#a-main-right), the whole site is cut from A main.

<div class="grid" markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098553708/B06327C3D2690A0B11CB25B42B918584AC23F756/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098553541/330A29DAA8AB8AABD509BD6FBCE6951572BFFCB0/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)
</div>

Default plant can be defended from banana by shooting over the left corner of right headshot.

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098553338/ECEE6AE9E9EBB253E329D0BDB4A925637909EE6B/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom1_5x }

## T-side

### Bins

#### From banana

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098992376/C3497EC915ADD7719C36F84B60E0CC71D2D4B2EA/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)
<figcaption>JT</figcaption>
</figure>

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098992183/5957FC8084B2E2FC674B6F81CD43E53975E4FAA7/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ width="50%" }

#### From long

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098987758/A3AF9608D37BC726EBABEA76336A3AF620FB80A3/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)
<figcaption>JT, aim at the branch, not the chair</figcaption>
</figure>

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098987554/029D5C7D16043613AE588AB8798BEE383BB462B8/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ width="50%" }

### Bank

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098988724/F368769A35F474B4E1DADA005D7E6A79E845C453/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom1_5x }
<figcaption>JT</figcaption>
</figure>

### Bridge

#### From alley

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098989209/22C594E77C39292E5902B224FDA67C1D69813BBF/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom1_5x }

#### From short

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098992792/2AF25ACED8BC6E0F11D4740C57DC12B320C62801/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom2x }

### Concrete

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098990437/4345421E9766577BA72519E601D436D83D46E7CF/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom2x }
<figcaption>Align horizontally with local minima of top part of the contour of the leaves. Align vertically with the rings of the Fernsehturm.</figcaption>
</figure>

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098990161/17455F8DB3189E83B8743A9BC787956CE6761136/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom1_5x width="50%" }

### Heaven

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098992000/CA5994E262B50C9B11249542C8830A17A072C927/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)
<figcaption>JT, using the provided crosshair, align inner gap with outer edge of the fence post.</figcaption>
</figure>
