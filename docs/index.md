# Potato CS2 Throwables

Welcome to my documentation of mostly my own research. All lineups are MIT licensed ([see below](#license)).

!!! tip

    Click on the pictures to open them in a slideshow format. Zoom may or may not work. To return from slideshow, press `[Ctrl+Esc]`

## Different throws and their initialisms

| Initialism | Meaning         | Details                                      |
|:----------:|-----------------|----------------------------------------------|
| T          | Regular throw   | Left-click                                   |
| JT         | Jump-throw      | Left-click and release when jumping          |
| RT         | Run-throw       | Usually done once reached full speed ~500ms  |
| WT         | Walk-throw      | Usually done once reached full speed ~1000ms |
| RJT        | Run-jump-throw  |                                              |
| WJT        | Walk-jump-throw |                                              |

## Feedback and bug reports

If you find any one-ways, inconsistencies, or tolerance issues among these lineups, or have suggestions for better lineups, please open an issue [here](https://gitlab.com/niklashh/cs2/-/issues).

## License

Copyright © 2023 Niklas "potato" Halonen

Permission is hereby granted, free of charge, to any person obtaining a copy of these lineups and associated documentation files (the “Lineups”), to deal in the Lineups without restriction, including without limitation the rights to throw, copy, jump-throw, modify, adjust, merge, publish, distribute, sublicense, and/or sell copies of the Lineups, and to permit persons to whom the Lineups are furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Lineups.

THE LINEUPS ARE PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, BURNS, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE LINEUPS OR THE USE OR OTHER DEALINGS IN THE LINEUPS.
