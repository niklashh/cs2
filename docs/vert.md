# Mirage Lineup Index

## CT-side

## T-side

<div class="radar-container">
<img src="https://static.wikia.nocookie.net/cswikia/images/3/35/De_vertigo_radar.png/revision/latest/scale-to-width-down/1000?cb=20230718120758" class="noglightbox" style="pointer-events:none;">

<a href="./vert_smokes.html#boost" style="top:52%;left:73%;" class="t-smoke"></a>
<a href="./vert_smokes.html#heaven" style="top:42%;left:71%;" class="t-smoke"></a>
</div>
