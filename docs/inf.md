# Inferno Lineup Index

## CT-side

<div class="radar-container">
<img src="https://static.wikia.nocookie.net/cswikia/images/1/11/CS2_inferno_radar.png" class="noglightbox" style="pointer-events:none;">

<a href="./inf_smokes.html#ramp" style="top:60%;left:43%;" class="ct-smoke"></a>
<a href="./inf_smokes.html#banana" style="top:53%;left:44%;" class="ct-smoke"></a>
<a href="./inf_smokes.html#banana-retake" style="top:27%;left:57%;" class="ct-smoke"></a>
<a href="./inf_smokes.html#quadnewbox" style="top:25.5%;left:43.5%;" class="ct-smoke"></a>
<a href="./inf_smokes.html#mid" style="top:65%;left:51%;" class="ct-smoke"></a>
</div>

## T-side

<div class="radar-container">
<img src="https://static.wikia.nocookie.net/cswikia/images/1/11/CS2_inferno_radar.png" class="noglightbox" style="pointer-events:none;">

<a href="./inf_smokes.html#coffin" style="top:14%;left:51%;" class="t-smoke"></a>
<a href="./inf_smokes.html#b-site-ct-from-alt-mid" style="top:23%;left:58%;" class="t-smoke"></a>

<a href="./inf_fires.html#quadnewbox" style="top:25.5%;left:43.5%;" class="fire"></a>
</div>
