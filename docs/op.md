# Overpass Lineup Index

!!! warning

    The radar here is from the CS:GO version of Overpass.

## CT-side

<div class="radar-container">
<img src="https://static.wikia.nocookie.net/cswikia/images/8/89/Cs2_overpass_radar.png" class="noglightbox" style="pointer-events:none;">

<a href="./op_smokes.html#monster" style="top:44%;left:81%;" class="ct-smoke"></a>
<a href="./op_smokes.html#a-main-right" style="top:24%;left:41.5%;" class="ct-smoke"></a>
<a href="./op_smokes.html#a-main-left" style="top:28%;left:45%;" class="ct-smoke"></a>

<a href="./op_hes.html#a-site-default" style="top:22%;left:44%;" class="he"></a>
</div>

## T-side

<div class="radar-container">
<img src="https://static.wikia.nocookie.net/cswikia/images/8/89/Cs2_overpass_radar.png" class="noglightbox" style="pointer-events:none;">

<a href="./op_smokes.html#bins" style="top:14%;left:54%;" class="t-smoke"></a>
<a href="./op_smokes.html#bank" style="top:13%;left:44%;" class="t-smoke"></a>
<a href="./op_smokes.html#concrete" style="top:36%;left:63%;" class="t-smoke"></a>
<a href="./op_smokes.html#bridge" style="top:37%;left:67%;" class="t-smoke"></a>
<a href="./op_smokes.html#heaven" style="top:24%;left:58%;" class="t-smoke"></a>

<a href="./op_fires.html#close-left" style="top:19%;left:32%;" class="fire"></a>
<a href="./op_fires.html#short" style="top:40%;left:73%;" class="fire"></a>
</div>
