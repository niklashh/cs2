# Ancient Lineup Index

## CT-side

<div class="radar-container">
<img src="./Ancient_Radar.png" class="noglightbox" style="pointer-events:none;">

<a href="./anc_smokes.html#doors" style="top:60%;left:79%;" class="ct-smoke"></a>
</div>

## T-side

<div class="radar-container">
<img src="./Ancient_Radar.png" class="noglightbox" style="pointer-events:none;">

<a href="./anc_smokes.html#top-mid" style="top:38%;left:47.5%;" class="t-smoke"></a>
<a href="./anc_smokes.html#ct-lane" style="top:22%;left:36%;" class="t-smoke"></a>
<a href="./anc_smokes.html#tunnel" style="top:33%;left:31%;" class="t-smoke"></a>
<a href="./anc_smokes.html#long" style="top:36%;left:80%;" class="t-smoke"></a>
<a href="./anc_smokes.html#short" style="top:32%;left:70.5%;" class="t-smoke"></a>
<a href="./anc_smokes.html#plat" style="top:19%;left:17%;" class="t-smoke"></a>

<a href="./anc_fires.html#cubby" style="top:47%;left:71%;" class="fire"></a>
</div>
