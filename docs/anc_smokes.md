# Ancient Smoke Lineups

## CT-side

### Doors

!!! todo

## T-side

### Top mid

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098997174/52D30D06977834D62D235D57C458BFE0A730B452/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom2x }
<figcaption>JT</figcaption>
</figure>

### CT lane

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098996974/6AF339D364E86AD495F013DEE698CECDCFDE1CFB/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom1_5x }
<figcaption>JT</figcaption>
</figure>

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098996790/7479119E33751EA0EFC3DAD1EBF4FDDD842CE66B/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ width="50%" }

### Long

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098996402/22D50AE82AA190124C430DC16B2AD42813973B74/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom1_5x }

<div class="grid" markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098996632/AF62220709F5C20096707A39D2E06405E450FB6B/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098996200/E98EFDE607034D54BBB58763B880C1FF44982061/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom1_5x }
</div>

### Short

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098995514/0DC9F0726C080A76A38DE57F689AC95120D54DCA/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)
<figcaption>JT</figcaption>
</figure>

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009098995727/10805F0FEC1D723675C45D72CBDF5BB71B2CD9A1/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ width="50%" }

### Tunnel

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009078083433/FE65DC41010E1E3CDB8849000804B8D28356F0AB/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom2x }
<figcaption>JT</figcaption>
</figure>

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009078083201/63EAFC6BB1DCAD8984B34F6D8F073B7F0CE28F72/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ width="50%" }

### Plat

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009078083825/892FAF2FEF0106C258FDC42B8D480390800D99AC/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom2x }
<figcaption>JT</figcaption>
</figure>

<div class="grid" markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2172484009078084012/988EAFE797333D00DE8D34B292F4121AE8379C92/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)

![](https://steamuserimages-a.akamaihd.net/ugc/2172484009078083639/D4AFA5639D51DF581F8FA5EE1050196C43D120C4/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)
</div>
