# Mirage Smoke Lineups

## CT-side

## T-side

### Upper connector

Useful for split B take

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2115062479282989922/58185A6539BCB8C84C517952E013F068DA7049CF/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom1_5x }
<figcaption>RJT, timing sensitive</figcaption>
</figure>

![](https://steamuserimages-a.akamaihd.net/ugc/2115062479282989684/833ACC3292B9FF722A4C79B0A04B8D2EB7334396/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ width="50%" }

### CT

<figure markdown>
![](https://steamuserimages-a.akamaihd.net/ugc/2115062479282990273/F4C8432A521CF32DD0A960964F44F702FAC22BBD/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ .zoom1_5x }
<figcaption>JT</figcaption>
</figure>

![](https://steamuserimages-a.akamaihd.net/ugc/2115062479282990102/37D87171285BACF41E8C608BAC725D255C5E06F7/?imw=5000&imh=5000&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false){ width="50%" }
