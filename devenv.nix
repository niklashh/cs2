{ pkgs, ... }:
{
  languages.python = {
    enable = true;
    venv.enable = true;
    venv.requirements = ''
      mkdocs==1.5.3
      mkdocs-material==9.4.7
      mkdocs-git-revision-date-localized-plugin==1.2.1
      mkdocs-glightbox==0.3.4
    '';
  };

  # https://devenv.sh/pre-commit-hooks/
  pre-commit.hooks = {
    shellcheck.enable = true;
    black.enable = true;
    markdownlint.enable = true;
  };
  pre-commit.settings.markdownlint.config = {
    # MD013/line-length - Line length
    MD013 = false;
    # MD045/no-alt-text - Images should have alternate text (alt text)
    MD045 = false;
    # MD033/no-inline-html - Inline HTML
    MD033 = false;
  };
}
